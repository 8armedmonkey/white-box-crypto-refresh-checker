package wbc.refresh;

public class CheckerApi {

    private VersionRepository versionRepository;
    private Checker checker;

    public CheckerApi(VersionRepository versionRepository) {
        this.versionRepository = versionRepository;
    }

    public void initChecker() {
        checker = new Checker(versionRepository.retrieveStoredVersion());
    }

    public boolean checkVersion(Version version) {
        if (checker == null) {
            throw new IllegalStateException("Checker is not initialized. Call initChecker to initialize Checker.");
        }
        return checker.checkVersion(version);
    }

    public Version getStoredVersion() {
        return versionRepository.retrieveStoredVersion();
    }

    public void updateStoredVersion(Version version) {
        versionRepository.updateStoredVersion(version);
        initChecker();
    }

}
