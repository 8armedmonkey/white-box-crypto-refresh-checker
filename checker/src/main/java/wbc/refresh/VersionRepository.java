package wbc.refresh;

public interface VersionRepository {

    Version retrieveStoredVersion();

    void updateStoredVersion(Version version);

}
