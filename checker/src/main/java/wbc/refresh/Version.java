package wbc.refresh;

public class Version {

    private final String name;
    private final boolean containsWbcRefresh;

    public Version(String name, boolean containsWbcRefresh) {
        this.name = name;
        this.containsWbcRefresh = containsWbcRefresh;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Version) {
            Version other = (Version) obj;
            return name.equals(other.name) && containsWbcRefresh == other.containsWbcRefresh;
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public boolean containsWbcRefresh() {
        return containsWbcRefresh;
    }

}
