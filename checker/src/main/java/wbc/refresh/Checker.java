package wbc.refresh;

public class Checker {

    private final Version version;

    public Checker(Version storedVersion) {
        this.version = storedVersion;
    }

    public boolean checkVersion(Version version) {
        return version.containsWbcRefresh() && !version.equals(this.version);
    }

}
