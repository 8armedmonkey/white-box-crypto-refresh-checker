package wbc.refresh;

import org.junit.Test;

import static org.junit.Assert.*;

public class TestVersion {

    @Test
    public void getName_name_returnName() {
        Version version = new Version("1.0.0", true);
        assertEquals("1.0.0", version.getName());
    }

    @Test
    public void containsWbcRefresh_true_returnTrue() {
        Version version = new Version("1.0.0", true);
        assertTrue(version.containsWbcRefresh());
    }

    @Test
    public void containsWbcRefresh_false_returnFalse() {
        Version version = new Version("1.0.0", false);
        assertFalse(version.containsWbcRefresh());
    }

    @Test
    public void equals_sameEverything_returnTrue() {
        Version v1 = new Version("1.0.0", true);
        Version v2 = new Version("1.0.0", true);

        assertTrue(v1.equals(v2));
    }

    @Test
    public void equals_sameName_returnFalse() {
        Version v1 = new Version("1.0.0", true);
        Version v2 = new Version("1.0.0", false);

        assertFalse(v1.equals(v2));
    }

    @Test
    public void equals_sameContainsWbcRefresh_returnFalse() {
        Version v1 = new Version("1.0.0", true);
        Version v2 = new Version("2.0.0", true);

        assertFalse(v1.equals(v2));
    }

    @Test
    public void equals_arbitraryObject_returnFalse() {
        Version v = new Version("1.0.0", true);
        Object o = new Object();

        assertFalse(v.equals(o));
    }

}
