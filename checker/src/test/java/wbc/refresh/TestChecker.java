package wbc.refresh;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestChecker {

    @Test
    public void currentVersionNoWbcRefresh_newVersionNoWbcRefresh() {
        Checker checker = new Checker(new Version("1.0.0", false));
        assertFalse(checker.checkVersion(new Version("2.0.0", false)));
    }

    @Test
    public void currentVersionNoWbcRefresh_newVersionWithWbcRefresh() {
        Checker checker = new Checker(new Version("1.0.0", false));
        assertTrue(checker.checkVersion(new Version("2.0.0", true)));
    }

    @Test
    public void currentVersionWithWbcRefresh_newVersionNoWbcRefresh() {
        Checker checker = new Checker(new Version("1.0.0", true));
        assertFalse(checker.checkVersion(new Version("2.0.0", false)));
    }

    @Test
    public void currentVersionWithWbcRefresh_newVersionWithWbcRefresh() {
        Checker checker = new Checker(new Version("1.0.0", true));
        assertTrue(checker.checkVersion(new Version("2.0.0", true)));
    }

    @Test
    public void sameVersion_nothingHappen() {
        Checker checker = new Checker(new Version("2.0.0", true));
        assertFalse(checker.checkVersion(new Version("2.0.0", true)));
    }

}
