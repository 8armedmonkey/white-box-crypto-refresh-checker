package wbc.refresh;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestNoVersion {

    @Test
    public void equality_withOtherNoVersion_returnTrue() {
        assertTrue(new NoVersion().equals(new NoVersion()));
    }

    @Test
    public void equality_withOtherNormalVersion_returnFalse() {
        assertFalse(new NoVersion().equals(new Version("1.0.0", true)));
    }

}
