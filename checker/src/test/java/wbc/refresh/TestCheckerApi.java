package wbc.refresh;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@SuppressWarnings("Convert2Lambda")
public class TestCheckerApi {

    private VersionRepository versionRepository;
    private CheckerApi checkerApi;

    @Before
    public void setUp() {
        versionRepository = mock(VersionRepository.class);
        checkerApi = new CheckerApi(versionRepository);

        doAnswer(new Answer<Void>() {

            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                when(versionRepository.retrieveStoredVersion())
                        .thenReturn(invocation.getArgumentAt(0, Version.class));

                return null;
            }

        }).when(versionRepository).updateStoredVersion(any(Version.class));
    }

    @Test
    public void checkVersion_returnTrue() {
        when(versionRepository.retrieveStoredVersion()).thenReturn(new Version("1.0.0", true));

        checkerApi.initChecker();
        checkerApi.checkVersion(new Version("2.0.0", true));
    }

    @Test
    public void checkVersion_returnFalse() {
        when(versionRepository.retrieveStoredVersion()).thenReturn(new Version("1.0.0", true));

        checkerApi.initChecker();
        checkerApi.checkVersion(new Version("2.0.0", false));
    }

    @Test(expected = IllegalStateException.class)
    public void checkVersion_trackerNotInitialized_throwIllegalStateException() {
        checkerApi.checkVersion(new Version("2.0.0", true));
    }

    @Test
    public void getStoredVersion_returnStoredVersion() {
        when(versionRepository.retrieveStoredVersion()).thenReturn(new Version("1.0.0", true));

        Version storedVersion = checkerApi.getStoredVersion();

        assertEquals("1.0.0", storedVersion.getName());
        assertEquals(true, storedVersion.containsWbcRefresh());
    }

    @Test
    public void updateStoredVersion_storedVersionUpdated() {
        checkerApi.initChecker();
        checkerApi.updateStoredVersion(new Version("2.0.0", true));

        verify(versionRepository).updateStoredVersion(eq(new Version("2.0.0", true)));
    }

    @Test
    public void updateStoredVersion_checkVersionInteraction() {
        when(versionRepository.retrieveStoredVersion()).thenReturn(new Version("1.0.0", false));

        Version newVersion = new Version("2.0.0", true);

        checkerApi.initChecker();
        checkerApi.updateStoredVersion(newVersion);

        assertFalse(checkerApi.checkVersion(newVersion));
    }

}
